import sys
from dateutil.parser import isoparse
from datetime import timedelta


class AutoTurnOff:

  def __init__(self, standby_power=5.0, standby_time=timedelta(minutes=4)):
    print('AutoTurnOff(standby_power = {} W, standby_time = {})'.format(standby_power, standby_time))
    self.standby_power = standby_power
    self.standby_time = standby_time
    self.state = {}

  def get_next_action(self, data):
    last_seen = isoparse(data['last_seen'])
    power = data['power']
    state = data['state']

    self.state['last_seen'] = last_seen

    status_msg = '{} {}'.format(last_seen.strftime('%Y-%m-%d %H:%M:%S'), state)
    if power > 0.0:
        status_msg += ' ({:.1f} W)'.format(power)

    if state == 'OFF':
        print('{}.'.format(status_msg))
        self.state['on_standby_since'] = None
        return

    if power >= self.standby_power:
        print('{}. devices are in use.'.format(status_msg, power))
        self.state['on_standby_since'] = None
        return

    on_standby_since = self.state.get('on_standby_since')
    if not on_standby_since:
        print('{}. devices went into standby.'.format(status_msg, power))
        self.state['on_standby_since'] = last_seen
        return

    on_standby_duration = last_seen - on_standby_since
    if on_standby_duration > self.standby_time:
        print('{}. devices should turn OFF now.'.format(status_msg))
        return 'OFF'

    print('{}. devices standby duration: {}'.format(status_msg, on_standby_duration))


# Move to unit tests.

print('========== SELFTEST 1 ==========')
atoff = AutoTurnOff(standby_power=5.0, standby_time=timedelta(minutes=4))
assert atoff.get_next_action({'last_seen': '2021-12-18T23:00:59+01:00', 'power': 50.0, 'state': 'ON'}) is None
assert atoff.get_next_action({'last_seen': '2021-12-18T23:10:59+01:00', 'power': 50.0, 'state': 'ON'}) is None
assert atoff.get_next_action({'last_seen': '2021-12-18T23:20:59+01:00', 'power': 5.0, 'state': 'ON'}) is None
assert atoff.get_next_action({'last_seen': '2021-12-18T23:23:59+01:00', 'power': 5.0, 'state': 'ON'}) is None
assert atoff.get_next_action({'last_seen': '2021-12-18T23:24:59+01:00', 'power': 5.0, 'state': 'ON'}) is None
assert atoff.get_next_action({'last_seen': '2021-12-18T23:25:59+01:00', 'power': 5.0, 'state': 'ON'}) is None  # should not turn off because not strictly below power threshold
assert atoff.get_next_action({'last_seen': '2021-12-18T23:26:12+01:00', 'power': 4.0, 'state': 'ON'}) is None  # below power threshold, but first time seen (countdown starts)
assert atoff.get_next_action({'last_seen': '2021-12-18T23:27:12+01:00', 'power': 4.0, 'state': 'ON'}) is None  # below power threshold, but not long enough (1 minute(s))
assert atoff.get_next_action({'last_seen': '2021-12-18T23:28:12+01:00', 'power': 4.0, 'state': 'ON'}) is None  # below power threshold, but not long enough (2 minute(s))
assert atoff.get_next_action({'last_seen': '2021-12-18T23:29:12+01:00', 'power': 4.0, 'state': 'ON'}) is None  # below power threshold, but not long enough (3 minute(s))
assert atoff.get_next_action({'last_seen': '2021-12-18T23:30:12+01:00', 'power': 4.0, 'state': 'ON'}) is None  # below power threshold, but not long enough (4 minute(s))
assert atoff.get_next_action({'last_seen': '2021-12-18T23:31:13+01:00', 'power': 4.0, 'state': 'ON'}) == 'OFF'  # below power threshold for strictly more than 4 minute(s)

print('========== SELFTEST 2 ==========')
atoff = AutoTurnOff(standby_power=5.0, standby_time=timedelta(minutes=4))
assert atoff.get_next_action({'last_seen': '2021-12-18T23:00:59+01:00', 'power': 0.0, 'state': 'ON'}) is None  # First measurement should never turn off

print('========== SELFTEST 3 ==========')
atoff = AutoTurnOff(standby_power=5.0, standby_time=timedelta(minutes=4))
assert atoff.get_next_action({'last_seen': '2021-12-18T23:00:59+01:00', 'power': 0.0, 'state': 'OFF'}) is None  # Nothing to be done when already OFF

print('========== SELFTEST 4 ==========')
atoff = AutoTurnOff(standby_power=5.0, standby_time=timedelta(minutes=4))
assert atoff.get_next_action({'last_seen': '2021-12-18T23:00:59+01:00', 'power': 0.0, 'state': 'ON'}) is None  # First measurement detected standby power
assert atoff.get_next_action({'last_seen': '2021-12-18T23:05:59+01:00', 'power': 0.0, 'state': 'ON'}) == 'OFF'  # below power threshold for long enough

print('========== SELFTEST 5 ==========')
atoff = AutoTurnOff(standby_power=5.0, standby_time=timedelta(minutes=4))
assert atoff.get_next_action({'last_seen': '2021-12-18T23:00:58+01:00', 'power': 1.0, 'state': 'ON'}) is None  # First measurement detected standby power
assert atoff.get_next_action({'last_seen': '2021-12-18T23:05:58+01:00', 'power': 0.0, 'state': 'OFF'}) is None  # Device was turned off in between
assert atoff.get_next_action({'last_seen': '2021-12-18T23:10:58+01:00', 'power': 1.0, 'state': 'ON'}) is None  # First measurement after power on should not never turn off
assert atoff.get_next_action({'last_seen': '2021-12-18T23:14:58+01:00', 'power': 1.0, 'state': 'ON'}) is None  # below power threshold, but not long enough
assert atoff.get_next_action({'last_seen': '2021-12-18T23:14:59+01:00', 'power': 1.0, 'state': 'ON'}) == 'OFF'  # below power threshold for strictly more than 4 minute(s)

print('========== SELFTEST ENDED SUCCESSFULLY ==========')
