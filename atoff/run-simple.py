#!/usr/bin/env python3

import json
import sys

import paho.mqtt.client as mqtt
from datetime import timedelta

from atoff import AutoTurnOff


BROKER_HOSTNAME = sys.argv[1]
PLUG_TOPIC = sys.argv[2]
STANDBY_POWER = float(sys.argv[3])
STANDBY_MINUTES = float(sys.argv[4])

atoff = AutoTurnOff(
    standby_power=STANDBY_POWER,
    standby_time=timedelta(minutes=STANDBY_MINUTES)
)


def on_connect(client, userdata, flags, rc):
    print('Connected to broker.', file=sys.stderr)

    # Subscribe in on_connect() to renew subscriptions on reconnect.
    client.subscribe(PLUG_TOPIC)


def on_message(client, userdata, msg):
    topic = msg.topic

    if topic == PLUG_TOPIC:
        data = json.loads(msg.payload.decode('utf-8'))
        if atoff.get_next_action(data) == 'OFF':
            client.publish(PLUG_TOPIC + '/set/state', 'OFF')


client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

client.connect(BROKER_HOSTNAME, 1883, 60)
rc = client.loop_forever()

print("loop terminated with rc = " + rc, file=sys.stderr)
