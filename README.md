# smarthome

## atoff: Auto turn-off zigbee power plug using MQTT when devices are in standby

The script works with zigbee2mqtt and requires the "last_seen" feature to be
turned on.  Further you need Zigbee power plus with power measuring, e.g.
Xiaomi Mi power plug or BlitzWolf BW-SHP13.


Install dependencies in Debian unstable:
```sh
sudo apt install python3 python3-paho-mqtt python3-dateutil
```

Alternatively, if you can't use apt, try pip:
```sh
pip install paho-mqtt python-dateutil
```

Usage:
```sh
./atoff/run-simple.py broker_hostname plug_mqtt_topic power_limit_watts time_limit_minutes
```

Run script to trigger power-off when consumption was below 8 Watts for 60 minutes:
```sh
./atoff/run-simple.py 192.168.0.123 zigbee2mqtt/xiaomi_plug 8.0 60
```
